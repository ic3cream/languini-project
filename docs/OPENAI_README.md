# In-Brief

Implementation of this APIand onlyrequired three segments of code after setup.

### Setup

To setup, we put openai into our requirements.txt
Then weput an OPENAI_API_KEY into our environment


### Implmentation

The following is the API feature to send and receive messages. To carry on a conversation, the
chatbot requiresa message history in the form of a list of dictionaries in the form: `[{"role": str, "content": str}]`

```python
completion = openai.ChatCompletion.create(  # get the chatbot response
            model="gpt-3.5-turbo", messages=message_history
        )
        response = (  # process the response for string content
            completion["choices"][0]
            if isinstance(completion, dict)
            else completion.choices[0].message
        )
```

That's all there is to it.
