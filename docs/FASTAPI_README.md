# Back-End API: FastAPI

## Functional Overview:

### Information flow:

User authentication is managed using the Galvanize JWTDown library.

When an authenticated user sends a POST request to the "/api/messages/" endpoint, the router gets the
current user account info, takes the `content: str` field from the UserMessage, extracts the token from
the request, adds `{"username": <current_user>, "role": "user", "time_stamp": datetime.now()}` to the message dictionary, and calls the MessageQueries.create method passing the message dictionary and the request token
as arguments into the query.

- MessageQueries.create takes the provided token and creates this dictionary:

```python
conversation_in = {
    "time_stamp": datetime.now(),
    "username": UserMessageIn["username"],
    tokens: [token]
}
```

- It then calls the ConversationQueries.create method passing conversation_in as its argument. - ConversationQueries.create calls ConversationQueries.get_one_by_token, passing the token as is argument. - ConversationQueries.get_one_by_token uses the token to query the database. If a converation exists that
  has the current token in its "tokens" field, it returns the info for it, else it returns `None`. - If the return is not `None`, ConversationQueries.create returns it, else it inserts conversation_in into the collection and calls ConversationQueries.get_one_by_token to get the conversation info and then returns that.
- MessageQueries.create then adds the conversation["_id"] field to the message dictionary and inserts that into the collection.
- It then calls self.get_one_by_id to returnthe message that was just created.

The router now takes the conversation_id from the message returned from create and uses that to call the MessageQueries.get_all_in_convo query with the conversation_id as its argument.

- MessageQueries.get_all_in_convo takes the provided id and queries the messages collection for all documents with the matching conversation_id key. It returns these results as a list with only the `content` and `role` fields.

The router takes the list of dictionaries and provides those as context for our OpenAI ChatCompletion request.
The Chatbot sends a response, from which router extracts the text, uses it to call MessageQueries.create again, this time creating a message document with the chatbot content and the `role` field "assistant".
Router finally returns a response to the front end with the chatbot content

### Endpoints

The MVP for this API requires the following endpoints:

- "/token" | methods: POST, GET, DELETE
- "/api/accounts" | methods: POST
- "/api/messages" | methods: POST

The following extra endpoints have been written and will be implemented as new features are designed:

- "/api/conversations" | methods: POST, GET
- "/api/conversations/{conversation_id}" | methods: GET, UPDATE, DELETE

## Structure

In the top-level directory of our api, we have the following folders:

`models | routers | queries | tests`

As well as the following loose files:

`Dockerfile | Dockerfile.dev | main.py | requirements.txt | token_auth.py`

### Models

In this folder we have .py files for accounts, conversations, messages, and a validator.

#### Models by file

| accounts.py            | converations.py             | messages.py           | validator.py     |
| :--------------------- | :-------------------------- | :-------------------- | :--------------- |
| SessionOut             | ConversationIn              | UserMessage           | PydanticObjectId |
| Account                | ConversationOut             | MessageIn             |
| AccountIn              | ConversationOutWithStringId | MessageOut            |
| AccountOut             |                             | MessageOutWithConvoId |
| AccountOutWithPassword |

- UserMessage is the expected form for a message coming to the router from the front-end, MessageIn is the form that MessageQueries expects from the router for new message documents.
- The PydanticObjectId class is designed to allow Pydantic type hinting for theMongoDB ObjectId class.
- The Account class is used within session queries, and uses the unmodified ObjectId, AccountOut requires the ObjectId to be converted to a string.

### Queries

| accounts.py | converations.py   | messages.py      | sessions.py     | client.py |
| :---------- | :---------------- | :--------------- | :-------------- | :-------- |
| get         | Create            | create           | get             | NO        |
| create      | get_one_by_token  | get_one_by_id    | create          | QUERIES   |
|             | get_one_by_id     | get_all_in_convo | delete          | ONLY      |
|             | update_one_tokens |                  | validate        | DB        |
|             | get_all           |                  | delete_sessions | SETUP     |
|             | delete_by_id      | completion



### Routers

| accounts.py | converations.py   | messages.py      | sessions.py     | client.py |
| :---------- | :---------------- | :--------------- | :-------------- | :-------: |
| get         | Create            | create           | get             |    NO     |
| create      | get_one_by_token  | get_one_by_id    | create          |  QUERIES  |
|             | get_one_by_id     | get_all_in_convo | delete          |   ONLY    |
|             | update_one_tokens |                  | validate        |    DB     |
|             | get_all           |                  | delete_sessions |   SETUP   |
|             | delete_by_id      |

### Tests
| test_converations.py   | test_messages.py |
|:-----------------------|:-------------------|
| test_create            | test_post_endpoint |
| test_get_one_by_token  |
| test_get_one_by_id     |
| test_update_one_tokens |
| test_get_all           |
| test_delete_by_id

###### Written by Andrew Casey 06SEP2023
