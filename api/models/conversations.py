from bson.objectid import ObjectId
from pydantic import BaseModel
from typing import List
from datetime import datetime


class ConversationIn(BaseModel):
    time_stamp: datetime
    username: str
    tokens: List[str]


class ConversationOut(ConversationIn):
    _id: ObjectId
