from pydantic import BaseModel
from datetime import datetime
from .validator import PydanticObjectId


# dictated by user
class UserMessage(BaseModel):
    content: str


# goes into database
class MessageIn(BaseModel):
    username: str
    time_stamp: datetime
    role: str
    content: str


# comes out of db, goes to chatbot
class MessageOut(BaseModel):
    role: str
    content: str


class MessageOutWithConvoId(MessageOut):
    convesersation_id: PydanticObjectId
