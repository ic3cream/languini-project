from bson.objectid import ObjectId
from models.conversations import ConversationIn, ConversationOut
from typing import List, Union
from pymongo.errors import DuplicateKeyError, InvalidOperation

# from models.messages import
from .client import Queries


class DuplicateConversationError(ValueError):
    pass


class ObjectIdChangeError(InvalidOperation):
    pass


class UpdateTokensError(Exception):
    pass


class ObjectIdError(ValueError):
    pass


class ConversationQueries(Queries):
    DB_NAME = "mongo-data"
    COLLECTION = "conversations"

    # TOKEN scenario
    # W16D3
    def create(self, conversation: ConversationIn) -> ConversationOut:
        # check if conversation associated with existing token exists
        check_conversation = self.get_one_by_token(conversation["tokens"][0])

        # if convo exists, add messages
        if check_conversation:
            return check_conversation
        # else, convo doesn't exist
        else:
            # insert conversation into conversations collection
            # or raise a duplicate error
            try:
                self.collection.insert_one(conversation)
            except DuplicateKeyError:
                raise DuplicateConversationError

            # when convo is pulled up, add token to convo tokens list
            conversation_out = self.get_one_by_token(conversation["tokens"][0])

            # Create a new message with the role "system" to prime the chatbot
            return conversation_out

    def get_one_by_token(
        self,
        token: str,
    ) -> ConversationOut | None:
        # logic to spit out ConversationOut | None
        # look within list, "tokens", to see if any value=token is in there
        conversation_out = self.collection.find_one(
            {"tokens": {"$elemMatch": {"$eq": token}}}
        )
        return conversation_out

    # W16D3
    def get_one_by_id(
        self,
        id: Union[str, ObjectId],
    ) -> ConversationOut:
        # grab existing conversation by specified id
        conversation_out = self.collection.find_one({"_id": ObjectId(id)})
        return conversation_out

    def update_one_tokens(self, id: str, token: str) -> ConversationOut:
        # get the selected conversation and extract its token list
        conversation = self.get_one_by_id(id)
        tokens = conversation["tokens"]

        # insert current token to first index in tokens and update the document
        tokens.insert(0, token)
        updated_conversation = self.collection.update_one(
            {"_id": ObjectId(id)}, {"$set": {"tokens": tokens}}
        )
        return updated_conversation

    def update_one_by_id(
        self, conversation_id: str, **kwargs
    ) -> ConversationOut:
        if "_id" in kwargs.keys():
            raise ObjectIdChangeError
        self.collection.update_one(
            {"_id": ObjectId(conversation_id)}, {"$set": kwargs}
        )
        conversation_out = self.collection.find_one(
            {"_id": ObjectId(conversation_id)}
        )
        return conversation_out

    # Used for displaying Conversation History
    def get_all(self) -> List[ConversationOut] | None:
        conversations = self.collection.find()
        conversations = [conversation for conversation in conversations]
        return conversations

    def delete_by_id(self, id: str) -> bool:
        success = self.collection.delete_one({"_id": ObjectId(id)})
        print(success.deleted_count)
        if success.deleted_count == 0:
            raise ObjectIdError
        return True
