from bson.objectid import ObjectId
from models.messages import MessageIn, MessageOut, MessageOutWithConvoId
from .conversations import ConversationQueries
from typing import List
from datetime import datetime
from pymongo.errors import DuplicateKeyError
from .client import Queries
from models.validator import PydanticObjectId
import openai


class DuplicateMessageError(ValueError):
    pass


class MessageQueries(Queries):
    DB_NAME = "mongo-data"
    COLLECTION = "messages"

    def create(
        self,
        UserMessageIn: MessageIn,
        token: str,
    ) -> MessageOutWithConvoId:
        # create "ConversationIn" object for ConversationQueries.create()
        conversation_in = {}
        conversation_in["time_stamp"] = UserMessageIn["time_stamp"]
        conversation_in["username"] = UserMessageIn["username"]
        conversation_in["tokens"] = [token]

        # find/create conversation based on token
        convo_queries = ConversationQueries()
        conversation = convo_queries.create(conversation=conversation_in)
        if len(
            self.get_all_in_convo(conversation_id=conversation["_id"])
        ) == 0:
            system_message = {}
            system_message["role"] = "system"
            system_message["username"] = conversation_in["username"]
            system_message["time_stamp"] = datetime.now()
            system_message[
                "content"
            ] = """
             You are a langauge learning partner.
             The user is learning a foreign language through the use of an
             application that takes spoken input through the a microphone,
             converts it into text, sends the text to you, receives
             the text back, and converts it to audio output for the user
             to hear. In this way, it forms a 1-on-1 conversation between
             the user and the AI. Through this conversation, the user is
             speaking to you, and you should respond as though you are
             speaking to the user.
"""
            self.collection.insert_one(system_message)

        # Assigning the new message's associated id to the conversation's id
        # UserMessageIn = UserMessageIn.dict()
        UserMessageIn["conversation_id"] = conversation["_id"]

        # add UserMessageIn to messages collection
        try:
            new_message = self.collection.insert_one(UserMessageIn)
        except DuplicateKeyError:
            raise DuplicateMessageError
        # prep output with role and content to feed to chatgpt
        # select the message in the db by it's ObjectId
        message_out = self.get_one_by_id(new_message.inserted_id)

        return message_out

    def get_one_by_id(self, id: str) -> MessageOutWithConvoId:
        message = self.collection.find_one({"_id": ObjectId(id)})
        message_out = {}
        message_out["role"] = message["role"]
        message_out["content"] = message["content"]
        message_out["conversation_id"] = message["conversation_id"]
        return message_out

    def get_all_in_convo(
        self,
        conversation_id: PydanticObjectId,
    ) -> List[MessageOut]:
        # find query and list comprehension for creating a list of all messages
        # with only "role" and "content" fields
        messages = self.collection.find(
            {"conversation_id": {"$eq": conversation_id}},
            {
                "_id": 0,
                "time_stamp": 0,
                "conversation_id": 0,
                "username": 0,
            },
        )
        return [message for message in messages]

    def completion(
        self,
        message_history,
    ):
        completion = openai.ChatCompletion.create(
            model="gpt-3.5-turbo", messages=message_history
        )
        response = (
            completion["choices"][0]
            if isinstance(completion, dict)
            else completion.choices[0].message
        )
        return response
