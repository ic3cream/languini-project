from fastapi.testclient import TestClient
from main import app
from pydantic import BaseModel
from routers.auth import authenticator
from models.messages import MessageIn
from queries.messages import MessageQueries
from bson.objectid import ObjectId
from models.accounts import AccountOut

client = TestClient(app)

# ARRANGE


class FakeAccountOut(BaseModel):
    id: str
    username: str
    full_name: str


class UserMessage(BaseModel):
    content: str


def fake_get_current_account_data():
    return AccountOut(id=1, username="Beau", full_name="Beau")


# mock function input
input = {
    "content": "1 + 1",
}


# Mock Repo
class FakeMessgaeQueries:
    def create(
        self,
        UserMessageIn: MessageIn,
        token: str,
    ):
        message_output = {
            "role": "user",
            "content": UserMessageIn,
            "conversation_id": ObjectId("64f9ed36ae716f086ab753ed"),
        }
        return message_output

    def get_all_in_convo(
        self,
        new_message: str,
    ):
        return [{"content": "1 + 1", "role": "user"}]

    def completion(
        self,
        message_history,
    ):
        result_2 = {
            "index": 0,
            "message": {"role": "assistant", "content": "1 + 1 equals 2."},
            "finish_reason": "stop",
        }
        return result_2


def test_post_messages():
    # ARRANGE

    # Dependency override section
    app.dependency_overrides[AccountOut] = FakeAccountOut
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_get_current_account_data
    app.dependency_overrides[MessageQueries] = FakeMessgaeQueries

    # ACT
    response = client.post("/api/messages", json=input)
    result = response.json()

    # ASSERT
    assert result == {"content": "1 + 1 equals 2."}
    assert response.status_code == 200

    # Clean Up
    app.dependency_overrides = {}
