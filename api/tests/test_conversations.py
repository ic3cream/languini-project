"""
Tests written for queries/conversations.py

Written by Andrew Casey 06SEP2023 feat. Peter Nguyen, who
wrote test_get_all as it appears in the final version

In this module, I used the mongomock library to mimick a limited-functionality
mongo databasefor the queries to run against. To test all the
onversationQueries methods I created a class named TestConversationQueries.
TestConversationQueries is-a TestCase and ConversationQueries.It overwrites
the @property which ConversationQueries inherits from queries.client.Queries.

This overwrite uses the fake_mongo client to setup a mock database and
collection so that when methods such as .create() call
self.collection.<method>, they have a mock-db collection to call methods on.

After defining the collection property and the tearDown method, I define a set
of objects to use in the various db operations.

Due to the function of mongomock, the data in these tests build on each other,
so thatthe test_get_all method will assert the existance of not only the
document placed into the db in that test, but also subsequent data used in
previous tests.

Due to the fact that ConversationQueries didn't require dependancy injection,
I did not need to override dependencies in writing these tests.
"""

from unittest import TestCase
import mongomock
from datetime import datetime
from queries.conversations import ConversationQueries as CQ
from bson.objectid import ObjectId


class TestConversationQueries(TestCase, CQ):
    fake_client = mongomock.MongoClient()

    @property
    def collection(self):
        db = self.fake_client["mockgo-data"]
        return db["conversations"]

    def tearDown(self):
        self.fake_client.close()

    objects = [
        dict(
            time_stamp=datetime.now(),
            username="testuser",
            tokens=[
                "ASDJFKASDJKFASLDFJFAFKFGJASFDHG",
                "OWEIERUTYQWOEUTUEIQWE",
            ],
        ),
        dict(
            time_stamp=datetime.now(),
            username="testuser2",
            tokens=["ZXMCNBNZXCMVZXCNV", "UIWERTIOQWEOITQWRQWU"],
        ),
        dict(
            time_stamp=datetime.now(),
            username="testuser3",
            tokens=["LASDJFGJASDKGDFAS"],
        ),
    ]

    def test_create(self):
        # Arrange
        conversation = self.objects[0]

        # Act
        new_conversation = self.create(conversation)

        # Assert
        assert isinstance(new_conversation["_id"], ObjectId)

    def test_get_one_by_token(self):
        # Arrange - blank

        # Act
        conversation1 = self.get_one_by_token(
            "ASDJFKASDJKFASLDFJFAFKFGJASFDHG"
        )
        conversation2 = self.get_one_by_token("OWEIERUTYQWOEUTUEIQWE")

        # Assert
        assert conversation1 == conversation2
        assert conversation1["username"] == "testuser"
        assert conversation1["tokens"] == [
            "ASDJFKASDJKFASLDFJFAFKFGJASFDHG",
            "OWEIERUTYQWOEUTUEIQWE",
        ]

    def test_get_one_by_id(self):
        # Arrange
        id = self.collection.find_one({"username": "testuser2"}, {"_id": 1})
        id = id["_id"]

        # Act
        conversation = self.get_one_by_id(str(id))

        # Assert
        assert conversation["username"] == "testuser2"

    def test_get_all(self):
        # Arrange
        self.collection.insert_one(self.objects[1])
        self.collection.insert_one(self.objects[2])

        # Act
        conversations = self.get_all()

        # Assert
        self.assertEqual(len(conversations), len(self.objects))
        for i, conversation in enumerate(conversations):
            self.assertEqual(
                conversation["username"], self.objects[i]["username"]
            )

    def test_update_one_tokens(self):
        # Arrange
        id = self.collection.find_one({"username": "testuser"}, {"_id": 1})

        # Act
        updated_conversation = self.update_one_tokens(
            id=str(id["_id"]), token="ASDJFKASDJKFUPDATEDASLDFJFAFKFGJASFDHG"
        )
        conversation = self.collection.find_one({"username": "testuser"})

        # Assert
        assert updated_conversation.matched_count == 1
        assert updated_conversation.modified_count == 1
        assert conversation["tokens"] == [
            "ASDJFKASDJKFUPDATEDASLDFJFAFKFGJASFDHG",
            "ASDJFKASDJKFASLDFJFAFKFGJASFDHG",
            "OWEIERUTYQWOEUTUEIQWE",
        ]

    def test_delete_by_id(self):
        # Arrange
        self.collection.insert_one(
            {
                "username": "testuser4",
                "tokens": ["UBBLEUBLEUBBLERNBBUBUEK"],
                "time_stamp": datetime.now(),
            }
        )
        id = self.collection.find_one({"username": "testuser4"}, {"_id": 1})

        # Act
        deleted = self.delete_by_id(id=str(id["_id"]))

        # Assert
        assert deleted is True
        new_id = self.collection.find_one(
            {"username": "testuser4"}, {"_id": 1}
        )
        assert new_id is None
