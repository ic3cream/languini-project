from fastapi import FastAPI
from fastapi.testclient import TestClient

app = FastAPI()
client = TestClient(app)


def fake_get_one_by_id(conversation_id: str):
    # Return a dummy conversation based on the given id.
    return {"id": conversation_id, "message": "test_message"}


@app.get("/conversations/{conversation_id}")
def get_one_conversation(conversation_id: str):
    return fake_get_one_by_id(conversation_id)


def test_get_one_conversation():
    test_conversation_id = "12345"
    response = client.get(f"/conversations/{test_conversation_id}")

    assert response.status_code == 200
    assert response.json()["id"] == test_conversation_id
    assert response.json()["message"] == "test_message"
