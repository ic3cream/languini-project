from fastapi import Depends, APIRouter, Request, HTTPException
from routers.auth import authenticator
from pydantic import BaseModel
from models.messages import UserMessage
from queries.messages import MessageQueries
from models.accounts import AccountOut
import os
import openai
from datetime import datetime


openai.api_key = os.environ.get("OPENAI_API_KEY")

router = APIRouter()


class ChatbotResponse(BaseModel):
    content: str


# POST
@router.post("/api/messages", response_model=ChatbotResponse, status_code=200)
async def user_message_in(
    message: UserMessage,
    request: Request,
    repo: MessageQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> ChatbotResponse:
    if account is None:  # Unlikely exception, but worth mitigating
        raise HTTPException(status_code=403, detail="User not authenticated")
    message = message.dict()
    token = request.cookies.get("fastapi_token")
    message["username"] = (
        account["username"] if isinstance(account, dict) else account.username
    )
    message["role"] = "user"
    message["time_stamp"] = datetime.now()
    new_message = repo.create(UserMessageIn=message, token=token)
    message_history = repo.get_all_in_convo(new_message["conversation_id"])
    # # CHAP GPT CHATBOT Call
    response = repo.completion(message_history)
    response_dict = (
        response["message"] if isinstance(response, dict) else response.message
    )
    response_dict["username"] = (
        account["username"] if isinstance(account, dict) else account.username
    )
    response_dict["role"] = "assistant"
    response_dict["time_stamp"] = datetime.now()
    repo.create(UserMessageIn=response_dict, token=token)

    return {"content": response_dict["content"]}
