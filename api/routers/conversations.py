from fastapi import Depends, APIRouter, Request, Response, HTTPException
from models.conversations import ConversationOut
from models.accounts import AccountOut
from queries.conversations import ConversationQueries
from typing import List, Dict
from .auth import authenticator
from datetime import datetime

router = APIRouter()


@router.get(
    "/api/conversations",
    response_model=List[ConversationOut],
    status_code=200,
)
async def get_conversations(
    repo: ConversationQueries = Depends(),
) -> List[ConversationOut]:
    conversations_out = repo.get_all()
    return conversations_out


@router.get(
    "/api/conversations/{conversation_id}",
    response_model=ConversationOut,
    status_code=200,
)
async def get_one(
    conversation_id: str, repo: ConversationQueries = Depends()
) -> ConversationOut:
    conversation = repo.get_one_by_id(conversation_id)
    return conversation


@router.post(
    "/api/conversations", response_model=ConversationOut, status_code=200
)
async def create_conversations(
    request: Request,
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
    repo: ConversationQueries = Depends(),
) -> ConversationOut:
    if account is None:  # Unlikely exception, but worth mitigating
        raise HTTPException(status_code=403, detail="User not authenticated")
    tokens = [request.cookies["fastapi_token"]]
    conversation = {}
    conversation["tokens"] = tokens
    conversation["username"] = (
        account["username"] if isinstance(account, dict) else account.username
    )
    conversation["time_stamp"] = datetime.now()
    conversation_out = repo.create(conversation=conversation)
    return conversation_out


@router.put(
    "/api/conversations/{conversation_id}",
    response_model=ConversationOut,
    status_code=200,
)
async def update_one_by_id(
    updates: Dict,
    conversation_id: str,
    repo: ConversationQueries = Depends(),
) -> ConversationOut:
    conversation_out = repo.update_one_by_id(conversation_id, updates)
    return conversation_out


@router.delete(
    "/api/conversations/{conversation_id}",
    status_code=204,
)
async def delete_conversation_by_id(
    conversation_id: str, repo: ConversationQueries = Depends()
) -> Response:
    success = {}
    success["deleted"] = repo.delete_by_id(conversation_id)
    return Response
