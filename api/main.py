from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from routers.auth import authenticator
from fastapi import APIRouter
from routers import accounts, messages, conversations


router = APIRouter()


origins = [
    "http://localhost:3000",
    os.environ.get("CORS_HOST", None),
]


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def read_root():
    return {"COMING": "SOON", "details": launch_details()}


app.include_router(authenticator.router)
app.include_router(accounts.router)
app.include_router(messages.router)
app.include_router(conversations.router)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 18,
            "day": 1,
            "hour": 19,
            "min": "00",
        }
    }
