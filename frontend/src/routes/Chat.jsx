import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";

const SpeechRecognition =
  window.SpeechRecognition || window.webkitSpeechRecognition;
const mic = new SpeechRecognition();
const synth = window.speechSynthesis;

mic.continuous = true;
mic.interimResults = true;

function Chat() {
  const navigate = useNavigate();
  const { fetchWithCookie } = useToken();
  const token = useToken();
  const [isListening, setIsListening] = useState(false);
  const [selectedLanguage, setSelectedLanguage] = useState("en-US"); // Default language
  const [audio, setAudio] = useState(null);
  const [savedUserChat, setSavedUserChat] = useState([]);
  const [savedBotChat, setSavedBotChat] = useState([]);
  const [botChat, setBotChat] = useState("");
  const [username, setUsername] = useState("");
  const [textBox, setTextBox] = useState("");

  const fetchUsername = async () => {
    const fetchUrl = `${process.env.REACT_APP_API_HOST}/token`;
    const userResponse = await fetchWithCookie(fetchUrl);
    if (userResponse) {
      const responseData = await userResponse.account.username;
      setUsername(responseData);
    } else {
      console.log("Error getting username", responseData);
    }
  };

  useEffect(() => {
    fetchUsername();
  }, []);

  useEffect(() => {
    if (!token) {
      navigate("/");
    }
  }, []);

  useEffect(() => {
    handleListen();
  }, [isListening, selectedLanguage]);

  const handleStartStop = async (event) => {
    event.preventDefault();
    if (isListening == false) {
      setIsListening(true);
    } else {
      setIsListening(false);

      setSavedUserChat([audio, ...savedUserChat]);

      const data = {};
      data.content = audio;
      setAudio("");
      const chatUrl = `${process.env.REACT_APP_API_HOST}/api/messages`;
      const fetchOptions = {
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetchWithCookie(chatUrl, "post", fetchOptions);
      if (response.content) {
        const responseData = await response;
        setSavedBotChat([responseData.content, ...savedBotChat]);
        setBotChat(responseData.content);
      } else {
        console.log(`audioResponse error: ${JSON.stringify(response)}`);
      }
    }
  };

  useEffect(() => {
    if (botChat != "") {
      const uttered = new SpeechSynthesisUtterance(botChat);
      uttered.lang = selectedLanguage;
      synth.speak(uttered);
    }
  }, [botChat]);

  const handleListen = () => {
    mic.lang = selectedLanguage;

    if (isListening) {
      mic.start();
      mic.onend = () => {
        console.log("continue..");
        mic.start();
      };
    } else {
      mic.stop();
      mic.onend = () => {
        console.log("Stopped Mic on Click");
      };
    }
    mic.onstart = () => {
      console.log("Mics on");
    };

    mic.onresult = (event) => {
      const transcript = Array.from(event.results)
        .map((result) => result[0])
        .map((result) => result.transcript)
        .join("");
      setAudio(transcript);
      mic.onerror = (event) => {
        console.log(event.error);
      };
    };
  };

  const handleLanguageChange = (language) => {
    setSelectedLanguage(language);
    setIsListening(false); // Stop listening when language changes
  };

  const handleTextChange = (event) => {
    event.preventDefault();
    setTextBox(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const chatUrl = `${process.env.REACT_APP_API_HOST}/api/messages`;
    setSavedUserChat([textBox, ...savedUserChat]);
    const data = {};
    data.content = textBox;
    const fetchOptions = {
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetchWithCookie(chatUrl, "post", fetchOptions);
    if (response.content) {
      const responseData = await response;
      setSavedBotChat([responseData.content, ...savedBotChat]);
      setBotChat(responseData.content);
    } else {
      console.log(`audioResponse error: ${JSON.stringify(response)}`);
    }
  };

  return (
    <>
      <div className="chat-background">
        <h1>Chat</h1>
        <div className="container">
          <div className="menu">
            <button
              className={isListening ? "listening" : ""}
              onClick={handleStartStop}
            >
              {!isListening ? "Start" : "Stop"}
            </button>
            <select
              className="dropdown-menu"
              value={selectedLanguage}
              onChange={(e) => handleLanguageChange(e.target.value)}
            >
              <option value="zh-Hans">Chinese (PRC)</option>
              <option value="zh-Hant">
                Chinese (Taiwan - Not a part of China)
              </option>
              <option value="en-US">English (US)</option>
              <option value="fr-FR">French (French)</option>
              <option value="de-DE">German (Germany)</option>
              <option value="el-EL">Greek (Greece)</option>
              <option value="'gu-IN'">Gujarati (India)</option>
              <option value="'hi-IN'">Hindi (India)</option>
              <option value="it-IT">Italian (Italian)</option>
              <option value="ja-JP">Japanese (Japan)</option>
              <option value="ko-KR">Korean (Korea)</option>
              <option value="la">Latin (Latin)</option>
              <option value="pt-BR">Portuguese (Brazil)</option>
              <option value="ru-RU">Russian (Russia)</option>
              <option value="es-ES">Spanish (Spain)</option>
              <option value="'sw-KE'">Swahili (Kenya)</option>
              <option value="fil-PH">Tagalog (Philippines)</option>
              <option value="th-TH">Thai (Thailand)</option>
              <option value="vi-VN">Vietnamese (Vietnam)</option>
              <option value="zu-ZA">Zulu (South Africa)</option>
            </select>
            <form>
              <button onClick={handleSubmit}>Send</button>
              <input
                type="text"
                value={textBox}
                name="textInput"
                placeholder="Type your message here..."
                onChange={handleTextChange}
              />
            </form>
          </div>
          <div className="box">
            <table>
              <tbody>
                <tr>
                  <th>{username && username}</th>
                </tr>
                <tr>
                  <td>
                    {savedUserChat &&
                      savedUserChat.map((chat, i) => {
                        return (
                          <p key={`${i}${Date.now()}`}>
                            {chat} <br />
                          </p>
                        );
                      })}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="box">
            <table>
              <tbody>
                <tr>
                  <th className="chat-title">Bertrand Bottingham</th>
                </tr>
                <tr>
                  <td>
                    {savedBotChat &&
                      savedBotChat.map((n, i) => {
                        return (
                          <p key={`${i}${Date.now()}`}>
                            {n}
                            <br />
                          </p>
                        );
                      })}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
}

export default Chat;
