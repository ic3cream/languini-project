import { ImageSlider } from "/src/components/ImageSlider";
import React from "react";
import { useState, useEffect } from "react";
import { FlagList } from "../components/FlagList";
import { Icon } from "@iconify/react";
import { useNavigate } from "react-router-dom";

const Home = () => {
  const navigate = useNavigate();
  const slides = [
    {
      url:
        "https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExM3M2dGQ1anI3MHA4aWt0NXV3ZGJ2NDA2MXc3aGd0ZHE0eHpkdWtxayZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/N9bXjXaWQcjBLGbQ0u/giphy.gif",
      title: "Functionality2",
      caption:
        "core functionality example: lanugage selection, audio input, ai response formation, bot audio speech synthesis",
    },
    {
      url:
        "https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExY242ODEweHkyMHZpeGZ3cXh6aWpldDU0M3M1empqeHZsZjFndmxzdCZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/OhwnWVJUYMilb2HCop/giphy.gif",
      title: "text-translation",
      caption: "example: text translation and speech synthesis by bot",
    },
    {
      url:
        "https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExcWwydHJlcHFpdTUzdmg1a3N0NnlvNTgwZGd3dXM2N3VwZXhodnc3diZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/MiKXJZ8PbrUDuXfm4q/giphy.gif",
      title: "Functionality1",
      caption:
        "Beta user test of app functionality. Microphone is enabled and sound is output from our API.",
    },
    {
      url:
        "https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExcXpnZmZqMDJ1ZHJxbWpyaWt6ZnI0endhenYwcXRwNXNpMXJ2Mnp4NCZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/WmFy92nMyL68H8Dpag/giphy.gif",
      title: "Phil",
      caption: "some of the freind's who've helped us along the way",
    },
    {
      url:
        "https://media4.giphy.com/media/3ndAvMC5LFPNMCzq7m/giphy.gif?cid=ecf05e47wmpklfwz3qhe5ep7nqry78qoer6vtsqhznlc34fq&ep=v1_gifs_gifId&rid=giphy.gif&ct=g",
      title: "Chatpage",
      caption: "Doggo",
    },
  ];

  const flags = {
    English:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/US.svg?ref_type=heads",
    French:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/FR.svg?ref_type=heads",
    German:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/DE.svg?ref_type=heads",
    Greek:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/GR.svg?ref_type=heads",
    Gujarati:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/IN.svg?ref_type=heads",
    Hindi:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/IN.svg?ref_type=heads",
    Italian:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/IT.svg?ref_type=heads",
    Japanese:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/JP.svg?ref_type=heads",
    Korean:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/KR.svg?ref_type=heads",
    Latin:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/IT.svg?ref_type=heads",
    Portuguese:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/BR.svg?ref_type=heads",
    Russian:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/RU.svg?ref_type=heads",
    Spanish:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/MX.svg?ref_type=heads",
    Swahili:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/KE.svg?ref_type=heads",
    Tagalog:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/PH.svg?ref_type=heads",
    Thai:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/TH.svg?ref_type=heads",
    Vietnamese:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/VN.svg?ref_type=heads",
    Zulu:
      "https://gitlab.com/catamphetamine/country-flag-icons/-/raw/master/3x2/ZA.svg?ref_type=heads",
  };

  const [greeting, setGreeting] = useState("Willkommen");
  const [greetingCount, setGreetingCount] = useState(0);
  const containerStyles = {
    width: "800px",
    height: "380px",
    margin: "0 auto",
  };

  const callSetGreetingCount = () => {
    if (greetingCount < 4) {
      setGreetingCount(greetingCount + 1);
    } else {
      setGreetingCount(0);
    }
  };

  const handleRedirect = () => {
    console.log()
    navigate("/signup")
  }

  useEffect(() => {
    setTimeout(callSetGreetingCount, 3000);
  }, [greeting]);

  useEffect(() => {
    if (greetingCount == 0) {
      setGreeting("Willkommen");
    } else if (greetingCount == 1) {
      setGreeting("Hola");
    } else if (greetingCount == 2) {
      setGreeting("你好");
    } else if (greetingCount == 3) {
      setGreeting("Xin chào");
    } else if (greetingCount == 4) {
      setGreeting("مرحبًا");
      setTimeout(() => {
        setGreetingCount(0);
      }, 3000);
    }
  }, [greetingCount]);

  useEffect(() => {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          entry.target.classList.add("show");
        } else {
          entry.target.classList.remove("show");
        }
      });
    });

    const hiddenElements = document.querySelectorAll(".hidden"); // Use a class selector
    hiddenElements.forEach((el) => observer.observe(el));

    return () => {
      // Cleanup the observer when the component unmounts
      hiddenElements.forEach((el) => observer.unobserve(el));
    };
  }, []);

  const Button = () => {
    return (
      <button className="enlarge-on-hover" onClick={handleRedirect}>
        <Icon icon="akar-icons:language" color="white" width="80" height="80" />{" "}
      </button>
    );
  };

  return (
    <>
      <div className="home">
        <div className="greeting">
          <h1>{greeting}</h1>
          <div>
            <p>Welcome to partnered language learning.</p>
          </div>
        </div>
        <div style={containerStyles}>
          <ImageSlider slides={slides} />
        </div>
      </div>

      {/* HERO GOES HERE */}
      <div className="mintBanner">
        <div className="hero hidden">
          <div>
            <FlagList flags={flags} />
          </div>
          <div className="description hidden">
            <div className="description-item hidden">
              Lango Tango makes it easier for people to learn new languages by
              supporting automatic speech recognition, chatGPT api
              responsiveness, and speech synthesis.
            </div>
            <div className="description-item hidden">
              Basically, it's an app that let's you speak to it in a foreign
              language and intelligently responds in that same language.
            </div>
            <div className="description-item hidden">
              Lango-Tango uses a react.js frontend with a FastAPI backend and
              MongoDB database. API's it calls include: ChatGPT 3.5 Turbo and
              Google Web Speech. Media is stored in an AWS S3 Bucket.
            </div>

            <div className="description-item hidden">Try it out now!</div>
              <Button />
          </div>
        </div>
      </div>
      <div className="blackBanner">
        <h2 className="techStackText">Utilized Tech Stack</h2>
        <div className="skills">
          <Icon icon="logos:react" width="70" height="70" />{" "}
          <Icon icon="logos:python" width="70" height="70" />{" "}
          <Icon icon="skill-icons:javascript" width="70" height="70" />{" "}
          <Icon icon="logos:fastapi" width="70" height="70" />{" "}
          <Icon icon="devicon:mongodb-wordmark" width="70" height="70" />{" "}
          <Icon icon="arcticons:openai-chatgpt" width="70" height="70" />{" "}
          <Icon icon="flat-color-icons:google" width="70" height="70" />{" "}
          <Icon icon="logos:aws-s3" width="70" height="70" />{" "}
        </div>
      </div>
      <div className="lavenderBanner">
        <div className="hidden">
          <div className="people">
            <div className="person hidden">
              <img
                className="dev-pics"
                src="https://lango-tango.s3.amazonaws.com/andrew.png"
                alt="andrew"
              ></img>

              <div className="dev-name">Andrew</div>
              <p>Fullstack Engineer / DevOps</p>
            </div>
            <div className="person hidden">
              <img
                className="dev-pics"
                src="https://lango-tango.s3.amazonaws.com/beau.png"
                alt="beau"
              ></img>
              <div className="dev-name">Beau</div>
              <p>Fullstack Engineer/ Designer</p>
            </div>

            <div className="person hidden">
              <img
                className="dev-pics"
                src="https://lango-tango.s3.amazonaws.com/hugo.png"
                alt="hugo"
              ></img>
              <div className="dev-name">Hugo</div>
              <p>Backend Engineer/ Quality Assurance</p>
            </div>

            <div className="person hidden">
              <img
                className="dev-pics"
                src="https://lango-tango.s3.amazonaws.com/peter.png"
                alt="peter"
              ></img>
              <div className="dev-name">Peter</div>
              <p>Frontend Integration Engineer / Security</p>
            </div>

            <div className="aboutUsText">
              <h2>About Us</h2>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
