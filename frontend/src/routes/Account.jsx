import React from "react";
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import "../App.css";
import "../styles/Account.css";

function Account() {
  const navigate = useNavigate();
  const { logout } = useToken();
  const handleLogout = () => {
    logout();
    navigate("/");
  };

  const { token } = useToken();

  return (
    <div className="account-background">
      <h1 className="Account">Account</h1>
      <div className="card1">
        <div>
          <div>
            {!token && (
              <button
                className="account-button1"
                onClick={() => navigate("/signup")}
              >
                Sign Up
              </button>
            )}
            {!token && (
              <button
                className="account-button1"
                onClick={() => navigate("/login")}
              >
                Login
              </button>
            )}
          </div>
          {token && (
            <button className="account-button1" onClick={handleLogout}>
              Logout
            </button>
          )}
        </div>
      </div>
    </div>
  );
}

export default Account;
