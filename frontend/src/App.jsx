import { useEffect, useState } from "react";
import Construct from "./Construct";
import ErrorNotification from "./ErrorNotification";
import { AuthProvider, useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import "./App.css";
import {
  BrowserRouter,
  Outlet,
  Route,
  Routes,
  Navigate,
} from "react-router-dom";
import Home from "./routes/Home";
import Accounts from "./routes/Account";
import Chat from "./routes/Chat";
import Navbar from "./components/Navbar";
import SignupForm from "./routes/Signup";
import LoginForm from "./routes/Login";
import Footer from "./components/Footer";

function App() {
  const [launchInfo, setLaunchInfo] = useState([]);
  const [error, setError] = useState(null);
  const baseUrl = process.env.REACT_APP_API_HOST;
  const [goTime, setGoTime] = useState(false);
  const domain = /https?:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");

  function ProtectedRoute({ element }) {
    const { token } = useAuthContext();

    if (!token) {
      return <Navigate to="/" replace />;
    }
    return element;
  }

  function UnprotectedRoute({ element }) {
    const { token } = useAuthContext();

    if (token) {
      return <Navigate to="/" replace />;
    }
    return element;
  }

  const timeCheck = () => {
    const now = Date.now();
    const target = Date.parse("11 Sep 2023 19:00:00.000Z");
    if (now >= target) {
      return true;
    } else {
      return false;
    }
  };

  useEffect(() => {
    async function getData() {
      let url = `${process.env.REACT_APP_API_HOST}/api/launch-details`;
      console.log("fastapi url: ", url);
      let response = await fetch(url);
      console.log("------- hello? -------");
      let data = await response.json();

      if (response.ok) {
        console.log("got launch data!");
        setLaunchInfo(data.launch_details);
      } else {
        console.log("drat! something happened");
        setError(data.message);
      }
    }
    getData();
    setGoTime(timeCheck());
  }, []);

  return (
    <>
      <div>
        <ErrorNotification error={error} />
        <BrowserRouter basename={basename}>
          <AuthProvider baseUrl={baseUrl}>
            <Navbar />
            <Outlet />
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/accounts" element={<Accounts />} />
              <Route
                path="/signup"
                element={<UnprotectedRoute element={<SignupForm />} />}
              />
              {/* <Route path="/signup" element={<SignupForm />} /> */}
              <Route path="/login" element={<LoginForm />} />
              <Route
                path="/chat"
                element={<ProtectedRoute element={<Chat />} />}
              />
              <Route
                path="/chatHist"
                element={<ProtectedRoute element={<chatHistory />} />}
              />
            </Routes>
          </AuthProvider>
        </BrowserRouter>
        {!goTime && <Construct info={launchInfo} />}
        {goTime && <Footer />}
      </div>
    </>
  );
}

export default App;
