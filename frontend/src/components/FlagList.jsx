const FlagList = ({ flags }) => {
  return (
    <div className="flags-container hidden">
      {Object.entries(flags).map(([name, url]) => (
        <div key={name} className="flag-item hidden">
          <img src={url} alt={name} />
          <h5>{name}</h5>
        </div>
      ))}
      ;
    </div>
  );
};

export { FlagList };
