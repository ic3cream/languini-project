import React, { useState } from "react";
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import { Link } from "react-router-dom";
import { SidebarData } from "./SidebarData";
import "../App.css";
import { IconContext } from "react-icons";
import useToken from "@galvanize-inc/jwtdown-for-react";

function Navbar() {
  const [sidebar, setSidebar] = useState(false);
  const { token } = useToken();

  const showSidebar = () => setSidebar(!sidebar);

  return (
    <>
      <IconContext.Provider value={{ color: "undefined" }}>
        <div className="navbar">

          <Link to="#" className="menu-bars">
            <FaIcons.FaBars onClick={showSidebar} />
          </Link><h2 className="a">LangoTango</h2>
        </div>
        <nav className={sidebar ? "nav-menu active" : "nav-menu"}>
          <ul className="nave-menu-items" onClick={showSidebar}>
            <li className="navbar-toggle">
              <Link to="#" className="menu-bars">
                <AiIcons.AiOutlineClose />
              </Link>
            </li>
            {token &&
              SidebarData.map((item, index) => {
                return (
                  <li key={index} className={item.cName}>
                    <Link to={item.path}>
                      {item.icons}
                      <span>{item.title}</span>
                    </Link>
                  </li>
                );
              })}
            {!token && (
              <li className={SidebarData[0].cName}>
                <Link to={SidebarData[0].path}>
                  {SidebarData[0].icons}
                  <span>{SidebarData[0].title}</span>
                </Link>
              </li>
            )}
            {!token && (
              <li className={SidebarData[2].cName}>
                <Link to={SidebarData[2].path}>
                  {SidebarData[2].icons}
                  <span>{SidebarData[2].title}</span>
                </Link>
              </li>
            )}
          </ul>
        </nav>
      </IconContext.Provider>
    </>
  );
}

export default Navbar;
